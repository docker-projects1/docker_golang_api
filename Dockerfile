FROM golang:1.15-alpine
MAINTAINER GINU MATHEW
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN go build -o main . 
CMD ["/app/main"]
